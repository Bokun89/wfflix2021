<?php

class Categories
{
    public function getAll()
    {
        $sql = "SELECT `id`, `name`, `description`, `createdAt` FROM `categories`";
        $categories = executeFetchAllSql($sql);

        if(true === !empty($categories)) :
            return $categories;
        endif;

        return false;
    }

    public function getById($id)
    {
        $sql = "SELECT `id`, `name`, `description`, `createdAt` FROM `categories` WHERE `id` = '$id'";
        $categories = executeFetchAllSql($sql);

        if(true === !empty($categories)) :
            return $categories;
        endif;

        return false;
    }
}