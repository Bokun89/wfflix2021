<?php

class Videos
{
    public function getAll()
    {
        $sql = "SELECT v.`id` as video_id, v.`name` as video_name, v.`category_id`, v.`link`, v.`description`, v.`user_id`, v.`views`, v.`createdAt`, c.`name` as category_name,  u.`id`, u.`username` as creator
                    FROM `videos` as v
                    INNER JOIN `categories` as c
                    INNER JOIN `users` as u
                    ON     v.`category_id` = c.`id`
                    AND    v.`user_id` = u.`id`";
        $videos = executeFetchAllSql($sql);

        if(!empty($videos)) :
            return $videos;
        endif;

        return false;
    }

    public function getBySearchTerm($search_term)
    {
        $sql = "SELECT `id` as video_id, `name` as video_name, `category_id`, `link`, `description`, `user_id`, `views`, `createdAt` FROM `videos` WHERE `name` LIKE '%$search_term%'";
        $videos = executeFetchAllSql($sql);

        if(!empty($videos)) :
            return $videos;
        endif;

        return false;
    }

    public function getByCategoryId($category_id)
    {
        $sql = "SELECT v.`id` as video_id, v.`name` as video_name, v.`category_id`, v.`link`, v.`description`, v.`user_id`, v.`views`, v.`createdAt`, c.`name` as category_name, u.`id`, u.`username` as creator
                    FROM `videos` as v
                    INNER JOIN `categories` as c
                    INNER JOIN `users` as u
                    ON     v.`category_id` = c.`id`
                    AND    v.`user_id` = u.`id`
                    AND    v.`category_id` = '$category_id'";
        $videos = executeFetchAllSql($sql);

        if(!empty($videos)) :
            return $videos;
        endif;

        return false;
    }

    public function getByUserId($user_id = 0)
    {
        if(empty($user_id)) :
            $user_id = getUserId();
        endif;

        $sql = "SELECT v.`id` as video_id, v.`name`, v.`category_id`, v.`link`, v.`description`, v.`user_id`, v.`views`, v.`createdAt`, c.`name` as category_name 
                    FROM `videos` as v 
                    INNER JOIN `categories` as c
                     ON v.`user_id` = '$user_id'
                     AND v.`category_id` = c.`id`";
        $videos = executeFetchAllSql($sql);

        if(!empty($videos)) :
            return $videos;
        endif;

        return false;
    }

    public function add($data)
    {
        $name           = $data['name'];
        $category_id    = $data['category_id'];
        $link           = $data['link'];
        $description    = $data['description'];
        $date           = mysqlDate();
        $user_id = getUserId();

        if(!empty($name) &&
            !empty($category_id) &&
            !empty($link) &&
            !empty($description) &&
            !empty($date) &&
            !empty($user_id)) :
            $sql = "INSERT INTO `videos` (`name`, `category_id`, `link`, `description`, `user_id`, `createdAt`) 
                        VALUES ('$name', '$category_id', '$link', '$description', '$user_id', '$date')";
            $added = executeSql($sql);

            if(true === $added) :
                return true;
            endif;

            return false;
        endif;

        return false;
    }

    public function remove($id)
    {
        $sql = "DELETE FROM `videos` WHERE `id` = '$id'";
        $removed = executeSql($sql);

        if(true === $removed) :
            return true;
        endif;

        return false;
    }

    public function updateViews($id)
    {
        $sql = "UPDATE `videos` SET `views` = `views`+1 WHERE `id` = '$id'";
        $updated = executeSql($sql);

        if(true === $updated) :
            return true;
        endif;

        return false;
    }
}