<?php

class User
{
    public static function login($username, $password)
    {
        global $conn;

        $user = [];
        $hashes_pwd = '';
        $sql = "SELECT `id`, `firstname`, `lastname`, `username`, `password` FROM `users` WHERE `username` = '$username'";

        if($query = $conn->query($sql)) :
            foreach ($query as $row) :
                $user['id']         =   $row['id'];
                $user['firstname']  =   $row['firstname'];
                $user['lastname']   =   $row['lastname'];
                $user['username']   =   $row['username'];

                $hashes_pwd = $row['password'];
            endforeach;

            if(!empty($user['id'])) :
                if($hashes_pwd === $password)
                {
                    return [true, $user];
                }
            endif;
        endif;

        //user not logged in
        return [false, 'De opgegeven gegevens komen niet overeen of gebruiker bestaat niet'];
    }
}