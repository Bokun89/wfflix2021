<?php

class CategoriesController
{
    public $categories;

    public function __construct()
    {
        $this->categories = new Categories();
    }

    public function index()
    {
        $content[] = '';
        $categories = $this->categories->getAll();

        if(true === !empty($categories)) :
            $content['categories'] = $categories;
        endif;

        view('categories', $content);
    }
}