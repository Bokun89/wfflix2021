<?php

class LoginController
{
    public function index()
    {
        view('login');
    }

    public function login()
    {
        $content = [];

        $username = htmlspecialchars($_POST['username']); $password = htmlspecialchars($_POST['password']);
        $user = User::login($username, $password);

        if(true == !empty($user)) :
            if(false === $user[0]) :
                $content['errorMessage'] = $user[1];

                view('shared/header');
                view('login', $content);
                return false;
            endif;

            $user = $user[1];

            if(true == !empty($user)) :
                $_SESSION['user_id']     =   (!empty($user['id'])) ? $user['id'] : '';
                $_SESSION['username']   =   (!empty($user['username'])) ? $user['username'] : '';

                return redirect('profile');
            endif;
        endif;

        $content['errorMessage'] = 'Er heeft zich een fout voor gedaan';
        view('shared/header');
        view('login', $content);
        return false;
    }
}