<?php

class VideosController
{
    public $videos;
    public $categories;

    public function __construct()
    {
        $this->videos = new Videos();
        $this->categories = new Categories();
    }

    public function index()
    {
        $content = [];

        if(isset($_GET['category']) && !empty($_GET['category'])) :
            $category_id = htmlspecialchars($_GET['category']);
            $videos = $this->videos->getByCategoryId($category_id);
            $categories = $this->categories->getById($category_id);

            if(!empty($videos[0]['category_name'])) :
                $category = $videos[0]['category_name'];
            else :
                $category = $this->categories->getById($category_id)[0]['name'];
            endif;

            if(!empty($category)) :
                $content['category'] = $category;
            endif;
        else :
            if(isset($_GET['search_term']) && !empty($_GET['search_term'])) :
                $search_term = htmlspecialchars($_GET['search_term']);

                $videos = $this->videos->getBySearchTerm($search_term);
            else :
                $videos = $this->videos->getAll();
            endif;

            $categories = $this->categories->getAll();
        endif;

        if(!empty($categories)) :
            $content['categories'] = $categories;
        endif;

        if(true === !empty($videos)) :
            $content['videos'] = $videos;
        endif;

        view('videos', $content);
    }

    public function updateViews()
    {
        if(!empty($_POST['video_id'])) :
            $video_id = htmlspecialchars($_POST['video_id']);
            $updated = $this->videos->updateViews($video_id);

            if(true === $updated) :
                echo json_encode(true);
                exit();
            endif;

            echo json_encode(false);
            exit();
        endif;

        echo json_encode(false);
        exit();
    }

    public function add()
    {
        $data = array(
            'name' => htmlspecialchars($_POST['name']),
            'link' => $_POST['link'],
            'category_id' => htmlspecialchars($_POST['category_id']),
            'description' => htmlspecialchars($_POST['description'])
        );

        if(!empty($data)) :
            $added = $this->videos->add($data);

            if(true === $added) :
                echo json_encode(true);
                exit();
            endif;

            echo json_encode(false);
            exit();
        endif;

        echo json_encode(false);
        exit();
    }

    public function remove()
    {
        if(!empty(htmlspecialchars($_POST['id']))) :
            $id = htmlspecialchars($_POST['id']);

            $removed = $this->videos->remove($id);

            if(true === $removed) :
                echo json_encode(true);
                exit();
            endif;

            echo json_encode(false);
            exit();
        endif;

        echo json_encode(false);
        exit();
    }
}