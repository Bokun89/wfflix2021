<?php

class ContactController
{
    public $contact;

    public function __construct()
    {
        $this->contact = new Contact();
    }

    public function index()
    {
        view('contact');
    }

    public function succesIndex()
    {
        view('contactSucces');
    }

    public function errorIndex()
    {
        view('contactError');
    }

    public function send()
    {
        $data = array(
            'name' => htmlspecialchars($_POST['name']),
            'email' => htmlspecialchars($_POST['email']),
            'message' => htmlspecialchars($_POST['message'])
        );

        if (!empty($data) && is_array($data) && !empty($data['name']) && !empty($data['email']) && !empty($data['message'])):
            $submission = $this->contact->create($data);

            if($submission) :
                redirect('contact-succes');
            else :
                return redirect('contact-error');
            endif;
        endif;
    }
}