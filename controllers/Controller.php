<?php

class Controller
{
    public static function view_exists($view)
    {
        try {
            if(!file_exists('views/'.$view.'.php')) :
                throw new Exception ('Pagina kon niet geopend of gevonden worden');
            endif;
        }
        catch(Exception $e) {
            require_once('views/404.php');
        }
    }
}