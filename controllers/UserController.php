<?php

class UserController
{
    public $videos;

    public function __construct()
    {
        $this->videos = new Videos();
    }

    public function index()
    {
        view('user/profile');
    }
    public function videosIndex()
    {
        $content = [];
        $videos = $this->videos->getByUserId();

        if(true === !empty($videos)) :
            $content['videos'] = $videos;
        endif;

        view('user/videos', $content);
    }

    public function logout()
    {
        session_destroy();
        return redirect('/');
    }
}