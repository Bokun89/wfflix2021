<?php

function getPageName($strip_slashes = false, $return_array = false)
{
    $page = $_SERVER['REQUEST_URI'];
    $page = stripslashes($page);

    if(true === $strip_slashes) :
        $page = str_replace('/', '', $page);
    endif;

    if(true === $return_array) :
        $page = explode('/', $page);

        if(true == !empty($page)) :
            return array_filter($page);
        endif;
    endif;

    return parse_url($page, PHP_URL_PATH);
}

function view($filename, $contents = array())
{
    if(false === file_exists('views/'.$filename.'.php')) :
        require_once('shared/404.php');
        exit();
    endif;

    if(true === is_array($contents) && true === !empty(($contents))) :
        extract($contents);
    endif;

    require_once('views/'.$filename.'.php');
}

function loadFilesFromDir($files, $directory)
{
    foreach($files as $file) :
        $fileinfo = pathinfo($file);

        if($file == '.' || $file == '..' || isset($fileinfo['extension']) && $fileinfo['extension'] !== 'php') :
            continue;
        endif;

        $file = $directory.'/'.$file;

        if(file_exists($file)) :
            require_once($file);
        endif;
    endforeach;
}

function redirect($location)
{
    header('location: '.$location);
}

function loadStyleForPage($page)
{
    $stylesheet = 'public/css/'.$page.'.css';

    if(file_exists($stylesheet)) :
        return '<link href="'.$stylesheet.'" rel="stylesheet"/>';
    endif;

    return false;
}

function checkIfIsAjaxCall()
{
    if(true === empty($_SERVER['HTTP_X_REQUESTED_WITH']))
    {
        return false;
    }

    return true;
}

/**
 * Can return customer_id or visitor_id
 *
 * @return false|string
 */
function getClientId($get_vistor = false)
{
    if(isset($_SESSION['userid']) && false === $get_vistor) :
        if(true == !empty(htmlspecialchars($_SESSION['userid']))) :
            return htmlspecialchars($_SESSION['userid']);
        endif;

        return false;
    else :
        if(false === isset($_COOKIE['visitor_id'])) :
            setVisitorId();
        endif;

        if(true === isset($_COOKIE['visitor_id'])) :
            if(true == !empty(htmlspecialchars($_COOKIE['visitor_id']))) :
                return htmlspecialchars($_COOKIE['visitor_id']);
            endif;
        endif;

        return false;
    endif;

    return false;
}

/**
 * Returns the actual id of the logged in user
 *
 * @return false|string
 */
function getUserId()
{
    if(isset($_SESSION['user_id'])) :
        if(true == !empty(htmlspecialchars($_SESSION['user_id']))) :
            return htmlspecialchars($_SESSION['user_id']);
        endif;

        return false;
    endif;

    return false;
}

function executeSql($query, $fetch_all = false)
{
    global $conn;

    if(true === $fetch_all) :
        $result = $conn->query($query)->fetchAll();

        if(true === !empty($result)) :
            return $result;
        endif;

        return false;
    endif;

    if($conn->query($query)) :
        return true;
    endif;

    return false;
}
function executeFetchAllSql($query)
{
    return executeSql($query, true);
}

function mysqlDate()
{
    return date('Y-m-d H:i:s');
}

function generateHash()
{
    $hash = '';
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    for ($i = 0; $i < 31; $i++)
    {
        $hash .= $characters[rand(0, 30)];
    }

    return $hash;
}

function isAdmin()
{
    if(isset($_SESSION['adminid'])) :
        if(!empty(htmlspecialchars($_SESSION['adminid']))) :
            return true;
        endif;

        return false;
    endif;

    return false;
}

function loggedIn()
{
    if(isset($_SESSION['user_id'])) :
        if(!empty(htmlspecialchars($_SESSION['user_id']))) :
            return true;
        endif;

        return false;
    endif;

    return false;
}

function setVisitorId($regenerate = false)
{
    if(true === $regenerate || empty($_COOKIE['visitor_id'])) :
        var_dump('test');

        setcookie('visitor_id', generateHash(), time() + (86400 * 30), '/');
    endif;
}

/**
 * todo: this function needs to run on a cron
 *
 * Removes left carts from visitors after 24h
 */
function emptyLeftOpenCarts()
{
    $carts = "SELECT `id`, `customer_id`, `createdAt` FROM `carts` WHERE HOUR(TIMEDIFF(NOW(), createdAt))>24";
    $carts = executeFetchAllSql($carts);

    $customer_ids = [];
    $all_customer_ids = "SELECT `id` FROM `customers`";
    $all_customer_ids = executeFetchAllSql($all_customer_ids);

    foreach($all_customer_ids as $customer)
    {
        unset($customer[0]);
        $customer_ids[] = $customer['id'];
    }

    if(true === !empty($carts)) :
        foreach ($carts as $cart) :
            $id = $cart['id'];

            if(!in_array($cart['customer_id'], $customer_ids)) :
                executeSql("DELETE FROM `carts` WHERE `id` = '$id'");
            endif;
        endforeach;
    endif;
}

function displayPrice($price, $decimals = 2)
{
    return number_format((float)$price, $decimals, ',', '');
}

function convertDate($date)
{
    return date('h:i d/m/Y', strtotime($date));
}

function getEnvironmentSrcPath($src_path = '/')
{
    if(false === strstr($_SERVER['HTTP_HOST'], 'localhost')) :
        $src_path = '/wfflix/';
    endif;

    return $src_path;
}

function last_array_item($array)
{
    $ite = '';

    foreach($array as $item)
    {
        $ite = $item;
    }

    return $ite;
}

function subFolderDomain()
{
    $subfolder = '';

    if(false === strstr($_SERVER['HTTP_HOST'], 'localhost')) :
        $subfolder = '/wfflix';
    endif;

    return $subfolder;
}
