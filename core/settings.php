<?php

session_start();

$page = last_array_item(getPageName(false, true));
$conn = DatabaseConfiguration::setConnection();

if(!defined('SITE_ROOT')) :
    if(strstr(subFolderDomain(), '/wfflix')) :
        define('SITE_ROOT', subFolderDomain());
    else :
        define('SITE_ROOT', '');
    endif;
endif;

/**
 * if not logged in as a user you're not allowed to visit these pages
 */
if(true === empty($_SESSION['user_id'])) :
    switch($page)
    {
        case 'profile' :
        case 'video-list' :
        case 'logout' :
            redirect('/');
            break;
    }
else :
    switch($page)
    {
        case 'login' :
        case 'register' :
            redirect('/');
            break;
    }
endif;