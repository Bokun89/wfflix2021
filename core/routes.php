<?php

$prefix = '';
$requestURI = explode('?', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))[0];
if (!empty($prefix) && $requestURI=='/'.$prefix):
    header("Location: /".$prefix."/", true, 301);
    exit();
elseif ($requestURI=='/'.$prefix."/"):
    $requestURI="/";
elseif (substr($requestURI, 0, strlen('/'.$prefix))===('/'.$prefix)):
    $requestURI=substr($requestURI, strlen('/'.$prefix));
endif;

$request = trim($requestURI, '/');

/**
 * We need this because production is a subfolder domain and not a fully qualified domain name
 */
if(strstr(subFolderDomain(), '/wfflix')) :
    $request = str_replace('wfflix/', '', $request);
endif;

if(!empty($_POST)) :
    switch($request)
    {
        case 'login' :
            Router::post('LoginController', 'login');
            break;

        case 'add-video' :
            Router::post('VideosController', 'add');
            break;
        case 'remove-video' :
            Router::post('VideosController', 'remove');
            break;
        case 'update-video-views' :
            Router::post('VideosController', 'updateViews');
            break;

        case 'contact' :
            Router::post('ContactController','send');
            break;

        default :
            view('shared/header', ['request' => $request]);
            view('shared/404');
            break;
    }
else :
    if(false === checkIfIsAjaxCall())
    {
        view('shared/header', ['request'=>$request]);
    }

    switch($request)
    {
        case '' :
        case '/' :
        case 'home' :
            Router::get('HomeController', 'index');
            break;
        case 'categories' :
            Router::get('CategoriesController', 'index');
            break;
        case 'videos' :
            Router::get('VideosController', 'index');
            break;
        case 'about' :
            Router::get('AboutUsController', 'index');
            break;
        case 'contact' :
            Router::get('ContactController', 'index');
            break;
        case 'contact-succes' :
            Router::get('ContactController','succesIndex');
            break;
        case 'contact-error' :
            Router::get('ContactController','errorIndex');
            break;


        case 'register' :
            Router::get('RegisterController', 'index');
            break;
        case 'login' :
            Router::get('LoginController', 'index');
            break;
        case 'logout' :
            Router::get('UserController', 'logout');
            break;

        case 'profile' :
            Router::get('UserController', 'index');
            break;
        case 'video-list' :
            Router::get('UserController', 'videosIndex');
            break;

        default :
            view('shared/404');
            break;
    }
endif;

if(false === checkIfIsAjaxCall())
{
    view('shared/footer');
}