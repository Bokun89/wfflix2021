<div class="inner-page-header">
    <h3 style="margin-left: 10%;">
        Over ons
    </h3>
</div>

<hr/>

<div class="grouppicca">
    <img src="<?php echo SITE_ROOT; ?>/public/src/groepsPicca.jpg" alt="Zieke groeps picca" width="30%" height="50%">
</div>
<div class="modal-dialog" style="text-align: center">
    <h6>Wij zijn de groep C2 van de ADSD opleiding in Almere Windesheim.
        Wij zijn gevraagd om een webapplicatie te creëren zodat studenten al het video materiaal kunnen vinden op één plek.
        De video's die hier te vinden zijn zullen voornamelijk bedoeld zijn voor  ICT opleidingen zoals ADSD en HBO ICT.
        C2 is een groep van vijf jongens Bryan, Enno, Joost Maurice en Remco. Wij zitten allemaal in het eerste jaar van ADSD
        van Windesheim Almere, in het verleden hadden we ook nog een ander teamlid maar niet is uit het groepje gestapt.
        Verder hebben we allemaal verschillende skills de we tijdens dit project hebben gebruikt, bijvoorbeeld
        Maurice's programeer skills, Enno's documentatie skills en Remco's oog voor design.
        Dit groepje was samen gevoegd tijdens de vierde week van het project na de eerste checkup van de week daarvoor.
        origineel bestond C2 maar uit twee personen Enno en het teamlid die uit de groep is gestapt vervolgens
        waren Bryan, Joost, Maurice en Remco er bij gekomen. Na een tijdje uitproberen en kennismaken hadden we allemaal
        onze eigen taken kunnen vinden waarmee we comfortabel waren. Vervolgens zijn hebben we onze moeite voornamelijk
        gestopt in Flevosap een project dat we tegelijker tijd hadden lopen, maar we hebben tijdens die periode
        meer ervaring opgedaan waardoor we met gemak Wfflix konden afronden.</h6>
</div>
