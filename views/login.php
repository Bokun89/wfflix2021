<div class="error-message"><?php echo (!empty($errorMessage)) ? $errorMessage : ''; ?></div>

<div class="login category-container">
    <h1 class="page-title" style="color:#000;">INLOGGEN</h1>

    <form method="post" action="login">
        <div class="login-container">
            <div class="login-box">
                <input type="text" id="username" name="username" class="login-field" placeholder="Email/Gebruikersnaam"/>
            </div>

            <div class="login-box">
                <input type="password" id="password" name="password" class="login-field" placeholder="Wachtwoord"/>
            </div>

            <div class="login-box">
                <input type="submit" id="login" class="button" name="login" value="Inloggen"/>
            </div>
        </div>
    </form>
</div>