<div class="inner-page-header">
    <h3 style="margin-left: 10%;">
        Contact
    </h3>
</div>

<hr/>

<div class="contact-page">
    <div class="row flex-lg-row-reverse align-items-center g-5 py-5">
        <div class="col-10 col-sm-8 col-lg-6">
            <form method="POST" action="contact" style="margin:0;">
                <div class="inputfield">
                    <p>Naam</p>
                    <input type="text" name="name" value size="40" aria-required="true" aria-invalid="false" height="40px" width="30%" placeholder="Naam" required>
                </div>
                <div class="inputfield">
                    <p>E-mail</p>
                    <input type="email" name="email" value size="40" aria-required="true" aria-invalid="false" placeholder="Email" required>
                </div>
                <div class="inputfield" width="56%">
                    <p>Bericht</p>
                    <textarea class="bericht"name="message" aria-required="true" aria-invalid="false" placeholder="Bericht" required></textarea>
                </div>
                <input type="submit" />
            </form>
        </div>
        <div class="col-lg-6">
            <h2 id="centreer">Wfflix contact</h2>
            <h5 id="centreer">For when you don't see what's next</h5>
        </div>
    </div>
</div>