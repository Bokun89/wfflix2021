<div class="inner-page-header">
    <h3 style="margin-left: 10%;">Alle categorieën</h3>
</div>

<hr/>

<div class="category-containers">
    <?php if(!empty($categories)) : foreach ($categories as $category) : ?>
        <div class="category-container">
            <input type="hidden" class="category_<?php echo $category['id']; ?>"/>
            <h4>
                <a href="videos?category=<?php echo $category['id']; ?>">
                    <?php echo $category['name']; ?>
                </a>
            </h4>

            <div class="category-inner">
                <?php echo $category['description']; ?>
            </div>

            <div class="category-footer" style="font-size: 12px;">
                <i><b>Gemaakt op:</b> <?php echo convertDate($category['createdAt']); ?></i>
            </div>
        </div>
    <?php endforeach; else: ?>
        Er zijn geen categorieën
    <?php endif; ?>
</div>