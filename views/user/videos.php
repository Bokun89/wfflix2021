<div class="inner-page-header">
    <h3 style="margin-left: 10%;">Mijn videos</h3>
    <span id="add_video" class="add-to">+</span>
</div>

<hr/>

<div id="profile_videos" class="category-containers">
    <?php if(!empty($videos)) : foreach ($videos as $video) : ?>
        <div id="video_<?php echo $video['video_id']; ?>" class="category-container" style="position:relative;">
            <span class="remove">X</span>

            <h4>
                <a href="<?php echo $video['link']; ?>" target="_blank">
                    <?php echo $video['name']; ?>
                </a>
            </h4>

            <div class="category-inner">
                <?php echo $video['description']; ?>
            </div><br/>

            <div class="category-footer" style="font-size: 12px;">
                <i><b>Categorie:</b> <?php echo $video['category_name']; ?></i><br/>
                <i><b>Gemaakt op:</b> <?php echo convertDate($video['createdAt']); ?></i>
            </div>
        </div>
    <?php endforeach; else: ?>
        <div class="nothing-found">
            Er zijn geen videos gevonden!
        </div>
    <?php endif; ?>
</div>