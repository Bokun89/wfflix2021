<div class="inner-page-header">
    <h3 style="margin-left: 10%;">
        <?php echo (!empty($category)) ? 'Categorie: '.$category : 'Alle videos'; ?>
        <?php echo (!empty($_GET['search_term'])) ? '| Zoekterm: '.htmlspecialchars($_GET['search_term']) : ''; ?>
    </h3>
</div>

<hr/>

<div class="category-parent">
    <div class="categories-left">
        <div id="filter_categories">
            <h5>Categorieën</h5>

            <?php if(!empty($categories)) : foreach ($categories as $category) : ?>
                <div>
                    <input type="checkbox" id="category_<?php echo $category['id']; ?>" value="<?php echo $category['name']; ?>" checked/>
                    <label for="category_<?php echo $category['name']; ?>"><?php echo $category['name']; ?></label>
                </div>
            <?php endforeach; endif; ?>
        </div>
    </div>

    <div class="categories-right category-containers" style="padding:0px;">
        <?php if(!empty($videos)) : foreach ($videos as $video) : ?>
            <div class="category-container">
                <input type="hidden" class="video-id" id="videoid_<?php echo $video['video_id']; ?>"/>
                <input type="hidden" class="category_<?php echo $video['category_id']; ?>"/>
                <h4>
                    <a href="<?php echo $video['link']; ?>" class="video-link" target="_blank">
                        <?php echo $video['video_name']; ?>
                    </a>
                </h4>

                <div class="category-inner">
                    <?php echo $video['description']; ?>
                </div><br/>

                <div class="category-footer" style="font-size: 12px;">
                    <i><b>Door:</b> <?php echo (!empty($video['creator'])) ? $video['creator'] : 'wfflix'; ?></i><br/>
                    <i><b>Aantal keer bekeken:</b> <?php echo $video['views']; ?></i><br/>
                    <i><b>Gemaakt op:</b> <?php echo convertDate($video['createdAt']); ?></i>
                </div>
            </div>
        <?php endforeach; else: ?>
            Er zijn geen videos gevonden!
        <?php endif; ?>
    </div>
</div>