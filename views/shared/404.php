<head>
    <style>
        h1 {text-align: center;}
        h6 {text-align: center;}
        p {text-align: center; color: #ffffff}
    </style>
</head>
<body id="background-algemeen" class="background-algemeen">

<div class="container error-404" id="404">
    <h1 class="page-title">NIET GEVONDEN</h1>


        <p>De opgevraagde pagina bestaat niet!</p> <br/>
        <p>Klik <a href="/home" style="color: #ffffff">hier</a> om terug naar de home pagina te gaan.</p>
</div>


</body>