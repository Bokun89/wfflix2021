<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- SEO Meta Tags -->
    <meta name="description" content="Opdracht wfflix 2021">
    <meta name="Team C3" content="Website WFflix">

    <!-- Nakijken of dit ook echt moet   -->
    <!--    &lt;!&ndash; OG Meta Tags to improve the way the post looks when you share the page on Facebook, Twitter, LinkedIn &ndash;&gt;-->
    <!--	<meta property="og:site_name" content="" /> &lt;!&ndash; website name &ndash;&gt;-->
    <!--	<meta property="og:site" content="" /> &lt;!&ndash; website link &ndash;&gt;-->
    <!--	<meta property="og:title" content=""/> &lt;!&ndash; title shown in the actual shared post &ndash;&gt;-->
    <!--	<meta property="og:description" content="" /> &lt;!&ndash; description shown in the actual shared post &ndash;&gt;-->
    <!--	<meta property="og:image" content="" /> &lt;!&ndash; image link, make sure it's jpg &ndash;&gt;-->
    <!--	<meta property="og:url" content="" /> &lt;!&ndash; where do you want your post to link to &ndash;&gt;-->
    <!--	<meta name="twitter:card" content="summary_large_image"> &lt;!&ndash; to have large image post format in Twitter &ndash;&gt;-->


    <!-- Webpage Title -->
    <title>WFflix 2021</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,400;0,600;0,700;1,400&family=Poppins:wght@600&display=swap" rel="stylesheet">
    <link href="public/elma/css/bootstrap.min.css" rel="stylesheet">
    <link href="public/elma/css/fontawesome-all.min.css" rel="stylesheet">
    <link href="public/elma/css/swiper.css" rel="stylesheet">
    <link href="public/elma/css/styles.css" rel="stylesheet">
    <script src="<?php echo getEnvironmentSrcPath('../../'); ?>public/elma/js/jquery.js"></script>

    <!-- Favicon = logo in je tab -->
    <link rel="icon" href="public/src/WFflixfavicon.png">
</head>
<body id="background-algemeen" class="background-algemeen <?php echo (!empty($request)) ? $request : 'home'; ?>-page" style="background-image: url('<?php echo SITE_ROOT; ?>/public/elma/images/header-background.jpg');">
<div id="notification_banner" class="notification-banner"></div>

<div id="modal" class="modal">
    <div class="custom-modal-content">
        <span class="close">&times;</span>
        <div id="modal_header" class="modal-header"></div>
        <div id="modal_body" class="modal-body"></div>
        <div id="modal_footer" class="modal-footer"></div>
    </div>
</div>

<!-- Navigation -->
<nav id="navbar" class="navbar navbar-expand-lg navbar-dark" aria-label="Main navigation">
    <div class="container col-11">

        <!-- Image Logo -->
        <a class="navbar-brand logo-image" href="homeView.html"><img src="public/src/WFflixlogoheadgroot.png" alt="alternative">
        </a>
        <!-- Searchbar dingen -->
        <form method="get" action="videos" class="container">
            <div class="row height d-flex justify-content-center align-items-center">
                <div class="col-md-6">
                    <input type="text" name="search_term" class="form-control" placeholder="De naam van de video">
                </div>
                <div class="col-md-6">
                    <button class="btn btn-primary">Search</button>
                </div>
            </div>
        </form>
        <!-- einde searchbar dingen -->
        <button class="navbar-toggler p-0 border-0"
                type="button"
                id="navbarSideCollapse"
                data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-label="Toggle navigation"
                aria-expanded="false"
        >
            <span class="navbar-toggler-icon">

            </span>
        </button>
        <!-- Navbar buttons href dingen aanpassen -->
        <div class="collapse navbar-collapse canvas-navbar" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto navbar-nav-scroll">
                <li class="nav-item">
                    <a class="nav-link <?= ($request=='home'||$request=='')?'active':''?>" aria-current="page" href="home">Home</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link <?= $request=='categories'?'active':''?>" href="categories">Categorieën</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= $request=='videos'?'active':''?>" href="videos">Video's</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= $request=='about'?'active':''?>" href="about">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= $request=='contact'?'active':''?>" href="contact">Contact</a>
                </li>

                <?php if(false === loggedIn()) : ?>
                <li class="nav-item">
                    <a class="nav-link <?= $request=='login'?'active':''?>" href="login">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= $request=='register'?'active':''?>" href="register">Register</a>
                </li>
                <?php endif; ?>
            </ul>

            <?php if(true === loggedIn()) : ?>
            <div id="hamburger_menu" class="navbar-nav ms-auto navbar-nav-scroll">
                <a href="profile" class="active title"><?php echo $_SESSION['username']; ?></a>

                <div id="sub_items" class="hamburger-items">
                    <a href="profile">Profiel</a>
                    <a href="video-list">Mijn videos</a>
                    <a href="logout">Uitloggen</a>
                </div>

                <a class="icon" style="cursor:pointer;margin-left: 15px;">
                    <i class="fa fa-bars hamburger-bar"></i>
                </a>
            </div>
            <?php endif; ?>
        </div> <!-- end of navbar-collapse -->
    </div> <!-- end of container -->
</nav> <!-- end of navbar -->
<!-- end of navigation -->

<div id="parent_body_container" class="parent-body-container">