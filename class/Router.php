<?php

class Router
{
    public static function get($controller, $method, $getter = null)
    {
        self::checkClassMethod($controller, $method);

        $request = new $controller();
        $request->$method($getter);
    }

    public static function post($controller, $method, $posts = null)
    {
        self::checkClassMethod($controller, $method);

        $request = new $controller();
        $request->$method($posts);
    }

    private static function checkClassMethod($controller, $method)
    {
        if(!class_exists($controller) || !method_exists($controller, $method)) :
            view('shared/404');
            view('shared/footer');
            exit;
        endif;
    }

}