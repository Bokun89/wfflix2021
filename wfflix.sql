-- auto-generated definition
create table categories
(
    id          int auto_increment
        primary key,
    name        varchar(50)                                                 not null,
    description varchar(255) default 'Geen beschrijving bij deze categorie' null,
    createdAt   datetime                                                    not null,
    constraint categories_name_uindex
        unique (name)
);

-- auto-generated definition
create table videos
(
    id          int auto_increment
        primary key,
    name        varchar(50)   not null,
    category_id int           not null,
    link        text          null,
    description text          null,
    user_id     int           not null,
    views       int default 1 null,
    createdAt   datetime      not null
);

-- auto-generated definition
create table users
(
    id        int auto_increment
        primary key,
    username  varchar(50) not null,
    password  text        not null,
    firstname varchar(50) not null,
    lastname  varchar(50) not null,
    updatedAt datetime    null,
    createdAt datetime    null,
    constraint users_username_uindex
        unique (username)
);

-- auto-generated definition
create table contact
(
    id        int auto_increment
        primary key,
    name      varchar(50) not null,
    email     text        not null,
    message   text        not null,
    createdAt datetime    not null
);

INSERT INTO wfflix.categories (id, name, description, createdAt) VALUES (1, 'PHP', 'Php is een hele leuk web taal en wordt voor 80% gebruikt op het web', '2021-10-30 13:08:14');
INSERT INTO wfflix.categories (id, name, description, createdAt) VALUES (2, 'Javascript', 'Javascript is leuk', '2021-10-30 13:45:16');
INSERT INTO wfflix.categories (id, name, description, createdAt) VALUES (3, 'CSS', 'CSS is helemaal kut', '2021-10-30 13:45:47');
INSERT INTO wfflix.categories (id, name, description, createdAt) VALUES (4, 'NodeJS', 'Geen beschrijving bij deze categorie', '2021-10-31 17:28:57');
INSERT INTO wfflix.categories (id, name, description, createdAt) VALUES (5, 'C#', 'Geen beschrijving bij deze categorie', '2021-10-31 17:29:14');
INSERT INTO wfflix.categories (id, name, description, createdAt) VALUES (6, 'MySQL', 'Geen beschrijving bij deze categorie', '2021-10-31 17:29:32');
INSERT INTO wfflix.categories (id, name, description, createdAt) VALUES (7, 'Burgerschap', 'Geen beschrijving bij deze categorie', '2021-10-31 17:29:46');
INSERT INTO wfflix.categories (id, name, description, createdAt) VALUES (8, 'Nederlands', 'Geen beschrijving bij deze categorie', '2021-10-31 17:29:56');
INSERT INTO wfflix.categories (id, name, description, createdAt) VALUES (9, 'Engels', 'Geen beschrijving bij deze categorie', '2021-10-31 17:30:02');
INSERT INTO wfflix.categories (id, name, description, createdAt) VALUES (10, 'Financiën', 'Geen beschrijving bij deze categorie', '2021-10-31 17:30:10');

INSERT INTO wfflix.videos (id, name, category_id, link, description, user_id, views, createdAt) VALUES (1, 'php training', 1, 'https://www.youtube.com/watch?v=OK_JCtrrv-c', 'Deze video gaat over het leren van PHP', 1, 1, '2021-10-30 14:15:15');
INSERT INTO wfflix.videos (id, name, category_id, link, description, user_id, views, createdAt) VALUES (2, 'javascript training', 2, 'https://www.youtube.com/watch?v=W6NZfCO5SIk', 'In deze video leer je javascript in 1 uur', 1, 1, '2021-10-30 14:26:31');
INSERT INTO wfflix.videos (id, name, category_id, link, description, user_id, views, createdAt) VALUES (5, 'javascript hard', 2, 'https://www.youtube.com/watch?v=PkZNo7MFNFg', 'Hier leer je javascript met veel kennis', 1, 1, '2021-10-31 11:25:29');
INSERT INTO wfflix.videos (id, name, category_id, link, description, user_id, views, createdAt) VALUES (16, 'Nederlands woordenschat', 8, 'https://www.youtube.com/watch?v=Ln-QS3ESeZY', 'Hier kan je beter je woordenschat oefenen', 2, 1, '2021-10-31 18:06:22');

INSERT INTO wfflix.users (id, username, password, firstname, lastname, updatedAt, createdAt) VALUES (1, 'Maurice038', 'test123', 'Maurice', 'Bruijn', '2021-10-31 09:19:43', '2021-10-31 09:19:48');
INSERT INTO wfflix.users (id, username, password, firstname, lastname, updatedAt, createdAt) VALUES (2, 'admin', 'admin', 'admin', 'admin', '2021-10-31 09:19:43', '2021-10-31 09:19:48');

INSERT INTO wfflix.contact (id, name, email, message, createdAt) VALUES (1, 'Maurice', 'maurice.bruijn@windesheim.nl', 'test', '2021-10-31 10:30:48');