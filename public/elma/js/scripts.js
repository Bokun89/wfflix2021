$(document).ready(function()
{
    let
        date = new Date(),
        hamburger_active = false,
        notification_banner = $('#notification_banner')
    ;

    $('.close').on('click', function()
    {
        $('#modal').hide();
    });

    $('.hamburger-bar').on('click', function()
    {
        if(hamburger_active === false)
        {
            hamburger_active = true;
            $('#sub_items').show().css('display', 'flex');
        }
        else
        {
            hamburger_active = false;
            $('#sub_items').hide();
        }
    });

    $('#profile_videos .remove').on('click', function()
    {
        let
            video = $(this).parent(),
            video_id = video.attr('id').replace('video_', '')
        ;

        $.ajax({
            type: 'POST',
            url: 'remove-video',
            data: {
                'id' : video_id
            },
            success: function(data)
            {
                notification_banner.text('De video kon niet verwijderd worden').css('background-color', '#e8c02c');

                if(data === 'true')
                {
                    notification_banner.text('De video is succesvol verwijderd').css('background-color', '#32c65c');
                    video.remove();
                }

                notification_banner.slideDown(1500).slideUp(4000);
            }
        });
    });

    $('#add_video').on('click', function()
    {
        $('#modal_header').text('Nieuwe video toevoegen');
        $('.modal-content').css('width', '70%').css('margin', '5% auto');
        $('#modal_body').empty().append(
            '<div class="input-container">' +
                '<label for="name" class="label">Naam:</label>' +
                '<input type="text" id="name" class="input-field" name="name"/>' +
            '</div>' +
            '<div class="input-container">' +
                '<label for="link" class="label">Link:</label>' +
                '<input type="text" id="link" class="input-field" name="link"/>' +
            '</div>' +
            '<div class="input-container">' +
                '<label for="categories" class="label">Categorie:</label>' +
                '<select id="categories" name="category">' +
                '<option value="1">PHP</option>' +
                '<option value="2">Javascript</option>' +
                '<option value="3">CSS</option>' +
                '<option value="4">NodeJS</option>' +
                '<option value="5">C#</option>' +
                '<option value="6">MySQL</option>' +
                '<option value="7">Burgerschap</option>' +
                '<option value="8">Nederlands</option>' +
                '<option value="9">Engels</option>' +
                '<option value="10">Financiën</option>' +
                '</select>' +
            '</div>' +
            '<div class="input-container">' +
                '<div class="label" for="description">Beschrijving:</div>' +
                '<textarea id="description" rows="5" style="width:100%;" name="description"></textarea>' +
            '</div>'
        );
        $('#modal_footer').empty().append(
            '<div id="save_video" class="button">Opslaan</div>'
        );

        $('#modal').show();

        $('#save_video').on('click', function()
        {
           let
               name = $('.input-container').find('#name').val(),
               link = $('.input-container').find('#link').val(),
               category = $('select[name=category] option').filter(':selected').text(),
               category_id = $('select[name=category] option').filter(':selected').val(),
               description =$('.input-container').find('#description').val(),
               posted_on = date.getHours() + ":" + date.getMinutes() +" "+ date.getDay() +"/"+ date.getMonth() +"/"+ date.getFullYear()
           ;

            $.ajax({
                type: 'POST',
                url: 'add-video',
                data: {
                    'name' : name,
                    'link' : link,
                    'category_id' : category_id,
                    'description' : description
                },
                success: function(data)
                {
                    notification_banner.text('De video kon niet toegevoegd worden').css('background-color', '#e8c02c');

                    if(data === 'true')
                    {
                        notification_banner.text('De video is succesvol toegevoegd').css('background-color', '#32c65c');
                        $('#modal').hide();
                        $('.nothing-found').remove();

                        $('#profile_videos').append(
                            '<div id="video_" class="category-container" style="position:relative;">' +
                            '<span class="remove">X</span>' +
                            '<h4>' +
                            '<a href="'+link+'" target="_blank">'
                            + name +
                            '</a>' +
                            '</h4>' +
                            '<div class="category-inner">'
                            + description +
                            '</div><br>' +
                            '<div class="category-footer" style="font-size: 12px;">' +
                            '<i><b>Categorie:</b> '+category+'</i><br/>' +
                            '<i><b>Gemaakt op:</b> '+posted_on+'</i>' +
                            '</div>' +
                            '</div>'
                        );
                    }

                    notification_banner.slideDown(1500).slideUp(4000);
                }
            });
        });
    });

    $('#filter_categories input[type=checkbox]').on('change', function()
    {
        let category = $(this).attr('id');

        if($(this).is(":checked"))
        {
            $('.category-containers .category-container .'+category).parent().show();
        }
        else
        {
            $('.category-containers .category-container .'+category).parent().hide();
        }
    });

    $('.video-link').on('click', function()
    {
        let
            video = $(this).parent().parent(),
            video_id = video.find('.video-id').attr('id').replace('videoid_', '')
        ;

        $.ajax({
            type: 'POST',
            url: 'update-video-views',
            data: {
                'video_id' : video_id
            },
            success: function(data)
            {
                console.log('test');
            }
        });
    });
});